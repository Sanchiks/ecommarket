package com.example.EcomMarket.controller;

import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.text.DecimalFormat;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.Callable;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.atomic.AtomicInteger;

public class ExerciseControllerCallable implements Callable<Double> {
    private final MockMvc mockMvc;
    private final String key;
    private final int countRequest;
    private final int requestParameter;
    private final long timeParameter;

    private long startTime;
    private long endTime;

    public ExerciseControllerCallable(MockMvc mockMvc, String key, int countRequest, int requestParameter, long timeParameter) {
        this.mockMvc = mockMvc;
        this.key = key;
        this.countRequest = countRequest;
        this.requestParameter = requestParameter;
        this.timeParameter = timeParameter;
    }

    @Override
    public Double call() throws Exception {
        return testForOneKey(mockMvc, key, countRequest, requestParameter, timeParameter);
    }

    /**
     * Выполняет countRequest запросов с ip key на контроллер, анализируя полученный ответ
     * @param key ip для генерации запроса
     * @param countRequest количество отправляемых запросов
     * @param requestParameter допустимое количество запросов за timeParameter
     * @param timeParameter период контроля количества запросов
     * @return процент запросов, с результатом, отличающимся от ожидаемого
     * @throws Exception при выполнении запроса
     */
    public double testForOneKey(MockMvc mockMvc, String key, int countRequest, int requestParameter, long timeParameter)
            throws Exception {
        startTime = System.currentTimeMillis();
        BlockingQueue<Long> queueTime = new LinkedBlockingQueue<>(requestParameter);
        // счетчик количества ошибок
        AtomicInteger resultIsFalse = new AtomicInteger(0);
        for (int i = 0; i < countRequest; i++) {
            long requestTime = System.currentTimeMillis();
            // оправляем запрос с заданным ip
            int status = mockMvc.perform(MockMvcRequestBuilders.get("/api/exercise/")
                            .with(request -> {
                                request.setRemoteAddr(key);
                                return request;
                            }))
                    .andReturn()
                    .getResponse()
                    .getStatus();

            if (queueTime.size() < requestParameter) {
                queueTime.offer(requestTime);
                equalsResult(200, status, resultIsFalse);
            } else {
                if ((requestTime - queueTime.peek()) <= (timeParameter))
                    equalsResult(502, status, resultIsFalse);
                else {
                    queueTime.remove();
                    queueTime.offer(requestTime);
                    equalsResult(200, status, resultIsFalse);
                }
            }
        }
        endTime = System.currentTimeMillis();
        double result = ((double) resultIsFalse.get() / countRequest) * 100.0;
        printResult(result);
        return result;
    }

    /**
     * Проверяет полученный код ответа с ожидаемым, увеличивает счетчик ошибок в случае несовпадения
     * @param exceptedCode ожидаемый код
     * @param rellyCode полученный код
     * @param resultIsFalse счетчик ошибки
     */
    private void equalsResult(int exceptedCode, int rellyCode, AtomicInteger resultIsFalse){
        if (exceptedCode != rellyCode){
            resultIsFalse.incrementAndGet();
        }
    }

    /**
     * Печать в консоль результата тестирования
     * @param result процент ошибочных запросов
     */
    private void printResult(double result){
        StringBuilder builder = new StringBuilder()
                .append("\n---Test for ip: ")
                .append(key)
                .append("\nCount request: ")
                .append(countRequest)
                .append("\nError: ")
                .append(new DecimalFormat("#0.00").format(result))
                .append("%\nExecute time: ")
                .append(endTime - startTime)
                .append("ms\n-------------------------");
        System.out.println(builder);
    }
}
