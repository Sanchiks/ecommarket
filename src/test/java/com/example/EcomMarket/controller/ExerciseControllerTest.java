package com.example.EcomMarket.controller;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import static com.example.EcomMarket.service.TimeConverter.convertTime;


@SpringBootTest
@AutoConfigureMockMvc
class ExerciseControllerTest {
    @Autowired
    private MockMvc mockMvc;

    /** Допустимая погрешность, задается в процентах **/
    private final static double ALLOWABLE_ERROR = 1.0;


    private final long timeParameter;
    private final int requestParameter;

    public ExerciseControllerTest( @Value("${throttling-service.time-parameter}") int timeParameter,
                                   @Value("${throttling-service.request-parameter}") int requestParameter) {
        this.timeParameter = convertTime(timeParameter);
        this.requestParameter = requestParameter;
    }

    @Test
    void exercise() throws Exception {
        int threadCount = 4;    // количество потоков для тестирования
        ExecutorService executorService = Executors.newFixedThreadPool(threadCount);
        List<ExerciseControllerCallable> callables = new ArrayList<>();
        callables.add(new ExerciseControllerCallable(mockMvc, "192.0.0.1", 4, requestParameter, timeParameter));
        callables.add(new ExerciseControllerCallable(mockMvc, "192.0.0.2", 100, requestParameter, timeParameter));
        callables.add(new ExerciseControllerCallable(mockMvc, "192.0.0.3", requestParameter + 1, requestParameter, timeParameter));
        callables.add(new ExerciseControllerCallable(mockMvc, "192.0.0.4", 100, requestParameter, timeParameter));
        callables.add(new ExerciseControllerCallable(mockMvc, "192.0.0.5", 100, requestParameter, timeParameter));
        callables.add(new ExerciseControllerCallable(mockMvc, "192.0.0.6", 100, requestParameter, timeParameter));
        callables.add(new ExerciseControllerCallable(mockMvc, "192.0.0.7", 100, requestParameter, timeParameter));
        callables.add(new ExerciseControllerCallable(mockMvc, "192.0.0.8", 10000, requestParameter, timeParameter));
        executorService.invokeAll(callables)
                .forEach(doubleFuture -> {
                    try {
                        Assertions.assertTrue(doubleFuture.get() <= ALLOWABLE_ERROR);
                    } catch (InterruptedException | ExecutionException e) {
                        throw new RuntimeException(e);
                    }
                });

    }
}