package com.example.EcomMarket;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EcomMarketApplication {

	public static void main(String[] args) {
		SpringApplication.run(EcomMarketApplication.class, args);
	}

}
