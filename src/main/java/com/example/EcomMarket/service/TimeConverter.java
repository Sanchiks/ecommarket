package com.example.EcomMarket.service;

public class TimeConverter {

    /**
     * Конвертирует минуты в миллисекунды
     * @param timeMinute минуты
     * @return миллисекунды
     */
    public static long convertTime(int timeMinute){
        return (long) timeMinute * 60 * 1000;
    }
}
