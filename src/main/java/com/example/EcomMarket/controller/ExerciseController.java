package com.example.EcomMarket.controller;


import com.example.EcomMarket.valid.Throttling;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.validation.Valid;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Validated
@RestController
@RequestMapping("/api/")
public class ExerciseController {

    @GetMapping(value = "/exercise/")
    public void exercise(@Valid @Throttling HttpServletRequest request) {
    }

}
