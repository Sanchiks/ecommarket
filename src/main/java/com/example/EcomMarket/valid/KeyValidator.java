package com.example.EcomMarket.valid;


import jakarta.servlet.http.HttpServletRequest;
import jakarta.validation.ConstraintValidator;
import jakarta.validation.ConstraintValidatorContext;

public class KeyValidator implements ConstraintValidator<Throttling, HttpServletRequest> {
    private final KeyThrottlingComponent keyThrottlingComponent;

    public KeyValidator(KeyThrottlingComponent keyThrottlingComponent) {
        this.keyThrottlingComponent = keyThrottlingComponent;
    }

    @Override
    public boolean isValid(HttpServletRequest value, ConstraintValidatorContext context) {
        return keyThrottlingComponent.checkRequest(value.getRemoteAddr());
    }

}
