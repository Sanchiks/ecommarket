package com.example.EcomMarket.valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.concurrent.*;

import static com.example.EcomMarket.service.TimeConverter.convertTime;

/**
 * Компонент для отслеживания частоты обращений с ключом.
 */
@Component
public class KeyThrottlingComponent {
    private final static Logger log = LoggerFactory.getLogger(KeyThrottlingComponent.class);

    /** Интервал контроля вызовов метода в мс */
    private final long timeParameterMs;
    /**  Количество допустимых запросов */
    private final int requestParameter;

    // мапа для хранения идентификатора и очереди с временем вызова метода
    private final ConcurrentHashMap<String, BlockingDeque<Long>> requestMap = new ConcurrentHashMap<>();

    public KeyThrottlingComponent(@Value("${throttling-service.time-parameter}") int timeParameter,
                                  @Value("${throttling-service.request-parameter}") int requestParameter) {
        this.timeParameterMs = convertTime(timeParameter);
        this.requestParameter = requestParameter;
    }

    /**
     * Удаляет из мапы информацию о вызовах для ключей, последнее обращение которых было больше чем timeParameterMs назад.
     * Вызывается по расписанию, удаление асинхронно в отдельном потоке.
     */
    @Scheduled(cron = "@hourly")
    private void clearMapAsync() {
        log.info("Starting a map cleanup\n Map size: {}", requestMap.size());
        CompletableFuture.runAsync(() -> {
            requestMap.entrySet()
                    .removeIf(entry -> entry.getValue().peekLast() < System.currentTimeMillis() - timeParameterMs);
            log.info("Finishing a map cleanup\n Map size: {}", requestMap.size());
        });
    }


    /**
     * Проверяет превышение количества вызовов метода с идентификатором key за период времени.
     * Параметры допустимого количества вызовов и период времени указаны в файле external.properties
     *
     * @param key идентификатор
     * @return истина если количество вызовов не превышает допустимое количество за период времени
     */
    public boolean checkRequest(String key) {
        // фиксируем время вызова метода
        long currentRequestTime = System.currentTimeMillis();
        if (requestMap.containsKey(key)) {
            return addElement(requestMap.get(key), currentRequestTime);
        } else {
            requestMap.put(key, createQueue(currentRequestTime));
            return true;
        }
    }

    /**
     * Создание новой блокирующей очереди с временем вызова метода
     *
     * @param requestTime первый элемент очереди
     * @return очередь
     */
    private BlockingDeque<Long> createQueue(long requestTime) {
        BlockingDeque<Long> deque = new LinkedBlockingDeque<>(requestParameter);
        deque.offerLast(requestTime);
        return deque;
    }

    /**
     * Добавление элемента в очередь в случае если очередь не заполнена или самый ранний элемент устарел
     *
     * @param queue       очередь
     * @param requestTime элемент со временем
     * @return истина, в случае успешного добавления элемента
     */
    private boolean addElement(BlockingDeque<Long> queue, long requestTime) {
        try {
            if (queue.size() < requestParameter) {
                queue.offerLast(requestTime);
            } else if (requestTime - queue.peekFirst() > timeParameterMs) {
                queue.removeFirst();
                queue.offerLast(System.currentTimeMillis());
            } else {
                return false;
            }
        } catch (NullPointerException e) {
            return false;
        }
        return true;
    }
}
