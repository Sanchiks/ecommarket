package com.example.EcomMarket.valid;

import jakarta.validation.Constraint;
import jakarta.validation.Payload;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.*;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

@Target(PARAMETER)
@Retention(RUNTIME)
@Constraint(validatedBy = KeyValidator.class)
@Documented
public @interface Throttling {
    String message() default "Превышено количество допустимых запросов.";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};

}
