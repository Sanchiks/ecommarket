FROM openjdk:17-jdk-alpine
COPY ["./external.properties", "./build/libs/EcomMarket-0.0.1-SNAPSHOT.jar", "./"]
CMD ["java","-jar","./EcomMarket-0.0.1-SNAPSHOT.jar"]